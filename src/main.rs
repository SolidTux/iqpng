#[cfg(feature = "chfft")]
use chfft::CFft1D;
#[cfg(feature = "fftw")]
use fftw::{
    array::AlignedVec,
    plan::{C2CPlan, C2CPlan32, Plan},
    types::{Flag, Sign},
};
use image::RgbImage;
use indicatif::{ProgressBar, ProgressStyle};
use iq::*;
use num_complex::Complex;
use num_traits::Zero;
use std::{
    collections::VecDeque,
    error::Error,
    fs::File,
    io::{Seek, SeekFrom},
    path::PathBuf,
    str::FromStr,
};
use structopt::StructOpt;

mod colors;
use colors::*;

#[derive(StructOpt)]
#[structopt(about, author)]
struct Opt {
    /// Path to raw IQ file
    filename: PathBuf,
    /// Data type, possible values are: i16, f32
    #[structopt(long = "type", short = "t", default_value = "i16", name = "type")]
    data: DataType,
    /// FFT size
    #[structopt(long, short, default_value = "4096")]
    fft: usize,
    /// Output filename without extension
    ///
    /// If omitted uses input filename plus ".png".
    #[structopt(long, short)]
    output: Option<String>,
    /// Step for FFT
    ///
    /// 0 means the same as the FFT size. If omitted, a quadratic image will be generated.
    #[structopt(long, short)]
    step: Option<usize>,
    /// Color map for image
    ///
    /// magma, viridis, plasma or bw
    #[structopt(long, short, default_value = "magma")]
    colormap: ColorMap,
    /// Use linear scale for colors
    #[structopt(long, short)]
    lin: bool,
}

enum ColorMap {
    Magma,
    Viridis,
    Plasma,
    BW,
}

impl FromStr for ColorMap {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "magma" => Ok(ColorMap::Magma),
            "viridis" => Ok(ColorMap::Viridis),
            "plasma" => Ok(ColorMap::Plasma),
            "bw" => Ok(ColorMap::BW),
            x => Err(format!("Unknown color map {x}")),
        }
    }
}

impl ColorMap {
    pub fn convert(&self, value: u8) -> [u8; 3] {
        match self {
            ColorMap::Magma => MAGMA[value as usize],
            ColorMap::Viridis => VIRIDIS[value as usize],
            ColorMap::Plasma => PLASMA[value as usize],
            ColorMap::BW => [value, value, value],
        }
    }
}

struct Fft {
    #[cfg(feature = "chfft")]
    fft: CFft1D<f32>,
    #[cfg(feature = "fftw")]
    plan: C2CPlan32,
    #[cfg(feature = "fftw")]
    i: AlignedVec<Complex<f32>>,
    #[cfg(feature = "fftw")]
    o: AlignedVec<Complex<f32>>,
}

impl Fft {
    #[cfg(feature = "arrayfire")]
    pub fn new(_: usize) -> Self {
        Fft {}
    }

    #[cfg(feature = "chfft")]
    pub fn new(size: usize) -> Self {
        Fft {
            fft: CFft1D::with_len(size),
        }
    }

    #[cfg(feature = "fftw")]
    pub fn new(size: usize) -> Self {
        Fft {
            plan: C2CPlan32::aligned(&[size], Sign::Forward, Flag::empty()).unwrap(),
            i: AlignedVec::new(size),
            o: AlignedVec::new(size),
        }
    }

    #[cfg(feature = "arrayfire")]
    pub fn process(&mut self, input: &[Complex<f32>], output: &mut [Complex<f32>]) {
        use arrayfire::*;

        let signal = Array::new(input, Dim4::new(&[input.len() as u64, 1, 1, 1]));
        let output_signal = fft(&signal, 1., input.len() as i64);
        output_signal.host(output);
    }

    #[cfg(feature = "chfft")]
    pub fn process(&mut self, input: &[Complex<f32>], output: &mut Vec<Complex<f32>>) {
        *output = self.fft.forward(input);
    }

    #[cfg(feature = "fftw")]
    pub fn process(&mut self, input: &[Complex<f32>], output: &mut [Complex<f32>]) {
        for i in 0..input.len() {
            self.i[i] = input[i];
        }
        self.plan.c2c(&mut self.i, &mut self.o).unwrap();
        for i in 0..input.len() {
            output[i] = self.o[i];
        }
    }

    #[cfg(not(any(feature = "chfft", feature = "arrayfire", feature = "fftw")))]
    pub fn process(&mut self, _: &[Complex<f32>], _: &mut [Complex<f32>]) {}
}

#[cfg(not(any(feature = "chfft", feature = "arrayfire", feature = "fftw")))]
compile_error!("No FFT feature is activated. Please choose either arrayfire or chfft.");

fn main() -> Result<(), Box<dyn Error>> {
    #[cfg(feature = "arrayfire")]
    {
        use arrayfire::{info, set_device};

        set_device(0);
        info();
    }

    let opt = Opt::from_args();
    let output_file = match opt.output {
        Some(s) => s,
        None => format!("{}.png", opt.filename.display()),
    };

    let mut f = File::open(opt.filename)?;
    let metadata = f.metadata()?;
    let step = match opt.step {
        Some(0) => opt.fft,
        Some(x) => x,
        None => (metadata.len() as usize / opt.data.bytes() - opt.fft) / (opt.fft - 1),
    };
    let len = 1 + (metadata.len() as usize / opt.data.bytes() - opt.fft) / step;

    let mut input: Vec<Complex<f32>> = vec![Complex::zero(); opt.fft];
    let mut output: Vec<Complex<f32>> = vec![Complex::zero(); opt.fft];

    let mut fft = Fft::new(opt.fft);

    let mut input_buffer = VecDeque::with_capacity(opt.fft);
    let mut buffer = Vec::new();

    let bar = ProgressBar::new(len as u64).with_style(ProgressStyle::default_bar().template(
        "{elapsed_precise} {wide_bar:.yellow/orange} {pos:>5}/{len:<5} ETA: {eta_precise}",
    )?);

    let (skip, take) = if step > opt.fft {
        (step - opt.fft, opt.fft)
    } else {
        (0, step)
    };

    bar.tick();
    for _ in 0..opt.fft {
        input_buffer.push_back(f.read_complex(&opt.data)?);
    }
    'outer: loop {
        for i in 0..opt.fft {
            input[i] = input_buffer[i];
        }
        fft.process(&input, &mut output);
        bar.inc(1);

        for i in 0..opt.fft {
            let x = output[(i + opt.fft / 2) % opt.fft];
            if opt.lin {
                buffer.push(x.norm());
            } else {
                buffer.push(x.norm().ln());
            }
        }

        if skip > 0 {
            f.seek(SeekFrom::Current((opt.data.bytes() * skip) as i64))?;
        }
        for _ in 0..take {
            match f.read_complex(&opt.data) {
                Ok(x) => {
                    input_buffer.pop_front();
                    input_buffer.push_back(x);
                }
                Err(_) => break 'outer,
            }
        }
    }
    bar.finish();

    let mut sorted = buffer.to_vec();
    sorted.sort_by(|a, b| a.partial_cmp(b).unwrap());
    let min = sorted[sorted.len() / 100];
    let max = sorted[sorted.len() * 99 / 100];
    let image_buffer = buffer
        .iter()
        .flat_map(|x| {
            opt.colormap
                .convert((255. * (x.max(min).min(max) - min) / (max - min)) as u8)
        })
        .collect::<Vec<_>>();

    let image = RgbImage::from_raw(
        opt.fft as u32,
        image_buffer.len() as u32 / 3 / opt.fft as u32,
        image_buffer,
    )
    .unwrap();
    image.save(output_file)?;

    Ok(())
}
