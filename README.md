# iqpng

Create waterfall PNG image from IQ recordings

## Installation

* Install [Rust](https://rustup.rs/)
* Run `cargo install --path .`

## Usage

```
    iqpng [FLAGS] [OPTIONS] <filename>

FLAGS:
    -h, --help       
            Prints help information

    -l, --lin        
            Use linear scale for colors

    -V, --version    
            Prints version information


OPTIONS:
    -f, --fft <fft>          
            FFT size [default: 4096]

    -o, --output <output>    
            Output filename without extension
            
            If omitted uses input filename plus ".png".
    -s, --step <step>        
            Step for FFT
            
            0 means the same as the FFT size. If omitted, a quadratic image will be generated.
    -t, --type <type>        
            Data type, possible values are: i16, f32 [default: i16]


ARGS:
    <filename>    
            Path to raw IQ file
```